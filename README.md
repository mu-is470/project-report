# Project Report

Group Project Milestone #3

Due: 12/13/21

Write a formal report that a) introduces the problem; b) describes the methodology and nature of the data; c) reports the results of your analysis; and d) include a conclusion/discussion. In addition, you will present your work as a team in a 10-minute presentation with 5 minutes of Q&A during our last class (12/13/21). This milestone is worth 70 points:
- Analysis/code: 30 points
- Written report: 20 points
- Presentation: 20 points
